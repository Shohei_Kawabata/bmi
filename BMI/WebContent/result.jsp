<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="model.Person" %>
<%
	DecimalFormat df = new DecimalFormat("###.00");
	Person person = (Person) session.getAttribute("person");
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>健康チェック</title>
</head>
<body>
	<h2>結果</h2>
	<p>お名前：<%= person.getName() %> 様<br>
	  年齢：<%= person.getAge() %> 歳<br>
	  身長：<%= person.getHealth().getHeight() %> cm<br>
	  体重：<%= person.getHealth().getWeight() %> kg<br>
	  診断：BMI=<%= df.format(person.getHealth().getBmi()) %>
		<%= person.getHealth().getMessage() %> </p>
	<table border="1">
		<tr style="background-color:steelblue; color: #ffffff">
			<th>BMI</th><th>体型</th>
		</tr>
		<tr><td>18.5未満</td><td>痩せ型</td></tr>
		<tr><td>18.5以上25未満</td><td>普通</td></tr>
		<tr><td>25以上</td><td>肥満</td></tr>
	</table>
	<br>
	<a href="index.jsp">先頭に戻る</a>
	<p><jsp:include page="date.jsp" /></p>
</body>
</html>