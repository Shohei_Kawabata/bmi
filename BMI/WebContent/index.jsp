<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String errmsg = (String) request.getAttribute("errmsg");
	if (errmsg == null) {
		errmsg = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>健康チェック</title>
</head>
<body>
	<h2>基本情報入力</h2>
	<form  action="healthcheck" method="post">
		<p>社員番号
			<input type="number" name="empno" style="width: 80px;" required><br>
		<span style="color: red;"><%= errmsg %></span><br>
		<p>
		<input type="submit" value="検索">
		<input type="reset">
		<input type="hidden" name="disp_id" value="G001">
		</p>
	</form>
</body>
</html>