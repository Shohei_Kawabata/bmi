<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="model.Person" %>
<%
	Person person = (Person) session.getAttribute("person");
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>健康チェック</title>
</head>
<body>
	ようこそ <%= person.getName() %>様
	<hr>
	<h2>身長と体重を入力してください</h2>
	<form action="healthcheck" method="post">
		<table class="input-form">
			<tr>
				<th>身長：</th>
				<td><input required id="height" name="height" type="text" pattern="^([1-9]\d*|0)(\.\d+)?$" value="">(cm)</td>
			</tr>
			<tr>
				<th>体重：</th>
				<td><input required id="weight" name="weight" type="text" pattern="^([1-9]\d*|0)(\.\d+)?$">(kg)</td>
			</tr>
		</table>
		<input type="submit"> <input type="reset">
		<input type="hidden" name="disp_id" value="G002">
	</form>
</body>

<style>
table {
	margin-bottom: 30px;
	text-align: left;
}
</style>
</html>