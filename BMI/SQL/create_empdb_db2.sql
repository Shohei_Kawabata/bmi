-- 社員DB作成
CREATE DB empdb USING CODESET UTF-8 TERRITORY JP;
-- 社員DBへ接続
CONNECT TO empdb USER db2admin;

-- 表削除
DROP TABLE employee;
DROP TABLE dept;

-- 部門表作成
CREATE TABLE dept (
	deptno INTEGER  NOT NULL
		GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1, NO CACHE)
		PRIMARY KEY,
	dept VARCHAR (15) NOT NULL
) ;
COMMIT;

-- 部門表にデータ挿入
INSERT INTO dept VALUES(DEFAULT, '開発部');
INSERT INTO dept VALUES(DEFAULT, '営業部');

-- 社員表作成
CREATE TABLE employee (
	empno INTEGER  NOT NULL
		GENERATED ALWAYS AS IDENTITY (START WITH 101, INCREMENT BY 1, NO CACHE)
		PRIMARY KEY,
	name VARCHAR (30) NOT NULL ,
	birthday DATE NOT NULL ,
	deptno INTEGER REFERENCES dept(deptno)
) ;

-- 社員表にデータ挿入
INSERT INTO employee (name,birthday,deptno) VALUES
	('中央　晴美', '1990-01-01', 1),
	('東　京子',   '1985-04-05', 2),
	('江戸　勝',   '1980-08-06', 1),
	('月島　有楽', '1995-10-31', 1);
COMMIT;

-- データ確認
SELECT * FROM dept;
SELECT * FROM employee;

CONNECT RESET;
