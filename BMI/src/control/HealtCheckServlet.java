package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpdbDao;
import model.Health;
import model.Person;

/**
 * リクエストを処理するサーブレット
 * @author ilearning
 */
@WebServlet("/healthcheck")
public class HealtCheckServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 文字コード
		request.setCharacterEncoding("UTF-8");

		// セッション取得
		HttpSession session = request.getSession();
		// セッションからpersonを取得
		Person person = (Person) session.getAttribute("person");

		// 画面IDの判定
		if("G001".equals((String)request.getParameter("disp_id"))){

			// 入力した社員番号で情報取得
			String empno = request.getParameter("empno");
			person = EmpdbDao.getEmployee(empno);

			// 取得出来なかった場合
			if (person == null) {
				request.setAttribute("errmsg", "一致する社員は見つかりません");
				request.getRequestDispatcher("index.jsp").forward(request, response);
				return;
			}

			// セッションに社員情報セット
			session.setAttribute("person", person);
			request.getRequestDispatcher("health.jsp").forward(request, response);

		} else if("G002".equals((String)request.getParameter("disp_id"))){
			if (person == null) {
				response.sendRedirect("index.jsp");
				return;
			}
			// 身長・体重取得
			double height = Double.parseDouble(request.getParameter("height"));
			double weight = Double.parseDouble(request.getParameter("weight"));

			// インスタンス生成
			Health health = new Health(height, weight);
			// personインスタンスにhealthインスタンスを内包
			person.setHealth(health);
			request.getRequestDispatcher("result.jsp").forward(request, response);
		} else {
			request.getSession().invalidate();
			response.sendRedirect("index.jsp");
		}
	}
}
