package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

	static {
		try {
			Class.forName("com.ibm.db2.jcc.DB2Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static final String DB_NAME = "metfrix";
	private static final String URL = "jdbc:db2://localhost:50000/" + DB_NAME;
	private static final String USER = "db2admin";
	private static final String PASSWORD = "password";

	protected static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(URL, USER, PASSWORD);
	}

}
