package dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Person;
/**
 * empdb用DAO
 * @author ilearning
 */
public class EmpdbDao extends ConnectionManager{

	// 定数
	private static final String DB_URL   = "jdbc:db2://localhost:50000/empdb";
	private static final String DB_USER  = "db2admin";
	private static final String PASSWORD = "password";
	private static final String JDBC_DRIVER = "com.ibm.db2.jcc.DB2Driver";

	// スタティックイニシャライザ
	// このクラスが最初に利用されるときに実行される処理
	static {
		try {
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			System.err.println("JDBCドライバのロードに失敗しました。");
			e.printStackTrace();
		}
	}

	/**
	 * 従業員番号で指定した従業員の情報を取得する
	 * @param empno 従業員番号
	 * @return	指定した従業員のオブジェクト。見つからない場合はnull。
	 */
	public static Person getEmployee(String empno) {
		// SQL
		final String SQL = "select * from employee where empno = cast(? as int)";

		// Person型の変数(DTO)を宣言
		Person person = null;
		try (Connection con = DriverManager.getConnection(DB_URL, DB_USER, PASSWORD);
			 PreparedStatement pstmt = con.prepareStatement(SQL) ) {
			// 社員番号をセット
			pstmt.setString(1, empno);
			// 結果セット取得
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				person = new Person();
				person.setName(rs.getString("name"));
				person.setBirthday(rs.getDate("birthday"));
			}
		} catch (SQLException e) {
			System.err.println("SQL:" + SQL);
			e.printStackTrace();
		}
		return person;
	}
}
