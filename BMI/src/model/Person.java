package model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * 入力値格納クラス
 * @author ilearning
 *
 */
public class Person implements Serializable {

	// フィールド
	private String name;
	private Date birthday;
	private Health health;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	/**
	 * Date型の引数をそのままフィールドにセットするセッター
	 * @param birthday
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	/**
	 * 生年月日の入力値を受け取り成型するセッター
	 * @param year
	 * @param month
	 * @param day
	 */
	public void setBirthday(int year, int month, int day) {
		// 生年月日の型となるインスタンス生成
		DateFormat df = new SimpleDateFormat("yyyy-M-d");

		// 引数をもとに値を設定
		try {
			this.birthday = df.parse(year + "-" + month + "-" + day);
		} catch (ParseException e) {
			System.err.println("生年月日：" + year + "-" + month + "-" + day + "が不正");
		}
	}
	public Health getHealth() {
		return health;
	}
	public void setHealth(Health helth) {
		this.health = helth;
	}

	/**
	 * フィールドの値から年齢を算出する
	 * @return 年齢
	 */
	public int getAge() {
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		int today = Integer.parseInt(df.format(new Date()));
		int birth = Integer.parseInt(df.format(birthday));
		return  (today - birth) / 10000;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", birthday=" + birthday + ", health=" + health + "]";
	}

}
