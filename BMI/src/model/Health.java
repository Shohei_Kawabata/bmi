package model;

import java.io.Serializable;

/**
 * 健康に関する情報を格納するクラス
 * @author ilearning
 */
public class Health implements Serializable {
	private static final long serialVersionUID = 1L;
	// フィールド
	private double height;
	private double weight;

	// 引数なしコンストラクタ
	public Health() { }
	// 引数ありコンストラクタ
	public Health(double height, double weight) {
		this.weight = weight;
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}

	/**
	 *
	 * BMIを算出
	 * @return
	 */
	public double getBmi() {
		if (height == 0) {
			return 0;
		}
		double material = height / 100;
		return weight / (material * material);
	}

	/**
	 *
	 * BMIに基づいたメッセージを返却
	 * @return 出力するメッセージ
	 */
	public String getMessage() {
		final double LOW  = 18.5;
		final double HIGH = 25.0;
		double bmi = getBmi();
		String message = "標準体型です。";
		if(bmi < LOW){
			message = "痩せすぎています。";
		} else if(bmi > HIGH){
			message = "太りすぎています。";
		}
		return message;
	}

	@Override
	public String toString() {
		return "Health [height=" + height + ", weight=" + weight + "]";
	}
}
